import { useEffect } from "react";
import { createContext, ReactNode, useContext, useState } from "react";
import api from "../../services/api";

interface Digimon {
  name: string;
  level: string;
  img: string;
}
interface DigimonsProviderData {
  digimons: Digimon[];
}

interface DigimonsProviderProps {
  children: ReactNode;
}

const DigimonsContext = createContext<DigimonsProviderData>(
  {} as DigimonsProviderData
);

export const DigimonsProvider = ({ children }: DigimonsProviderProps) => {
  const [digimons, setDigimons] = useState<Digimon[]>([] as Digimon[]);

  useEffect(() => {
    api.get("digimon").then((response) => setDigimons(response.data));
  }, []);

  return (
    <DigimonsContext.Provider value={{ digimons }}>
      {children}
    </DigimonsContext.Provider>
  );
};

export const useDigimons = () => useContext(DigimonsContext);
