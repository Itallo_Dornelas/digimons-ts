import { ReactNode } from "react";
import { DigimonsProvider } from "./digimons";
import { FavoriteDigimonsProvider } from "./favorites";

interface ProvidersProps {
  children: ReactNode;
}

const Providers = ({ children }: ProvidersProps) => {
  return (
    <DigimonsProvider>
      <FavoriteDigimonsProvider>{children}</FavoriteDigimonsProvider>
    </DigimonsProvider>
  );
};
export default Providers;
