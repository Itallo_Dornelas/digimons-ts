import { useDigimons } from "../../providers/digimons";
import { useFavoriteDigimons } from "../../providers/favorites";
import { ContainerCard, ContainerCards, Image } from "./styles";

const DigimonCard = () => {
  const { digimons } = useDigimons();
  const { addDigimon } = useFavoriteDigimons();

  return (
    <>
      <h2>All Digimons</h2>
      <ContainerCards>
        {digimons.map((digimon, index) => {
          return (
            <ContainerCard key={index}>
              <h2>{digimon.name}</h2>
              <h4>{digimon.level}</h4>
              <Image src={digimon.img} />
              <button onClick={() => addDigimon(digimon)}>Add Favorites</button>
            </ContainerCard>
          );
        })}
      </ContainerCards>
    </>
  );
};

export default DigimonCard;
