import styled from "styled-components";
export const ContainerCards = styled.div`
  display: flex;
  width: 100%;
  gap: 20px;
  overflow-x: scroll;
  &::-webkit-scrollbar {
    width: 10px;
    height: 5px;
  }

  ::-webkit-scrollbar-track {
    background: var(--white);
  }

  ::-webkit-scrollbar-thumb {
    background: var(--orange);
  }

  ::-webkit-scrollbar-thumb:hover {
    background: var(--gray);
  }
`;
export const ContainerCard = styled.div`
  width: 200px;
  padding: 10px;
  margin: 0 20px 10px;
  display: flex;
  flex-direction: column;
  align-items: center;
  background-color: var(--orange);
  border: 0.5px solid #ddd;
  border-radius: 20px;
  color: var(--blue);
  button {
    margin-top: 10px;
    color: var(--white);
    letter-spacing: 0.1rem;
    text-transform: uppercase;
    background: #66a2f1;
    background: -webkit-linear-gradient(to right, #2193b0, #66a2f1);
    background: linear-gradient(to right, #2193b0, #66a2f1);
    padding: 1rem 2rem;
    border-radius: 5px;
    cursor: pointer;
    border: none;

    :hover {
      color: rgba(0, 0, 0, 0.6);
    }
  }
`;

export const Image = styled.img`
  width: 70px;
  height: 70px;
  border-radius: 30px;
  object-fit: cover;
`;
