import { DivHeader } from "./styles";

const Header = () => {
  return (
    <DivHeader>
      <h1>Digimons - TS </h1>
    </DivHeader>
  );
};

export default Header;
