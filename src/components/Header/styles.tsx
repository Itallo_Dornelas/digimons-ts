import styled from "styled-components";

export const DivHeader = styled.div`
  width: 100%;
  background-color: var(--blue);
  text-align: center;
  background-image: linear-gradient(62deg, var(--red) 0%, var(--yellow) 100%);
  box-shadow: 0.2rem 0.3rem 2px rgba(0, 0, 0, 0.6);
  h1 {
    color: var(--blue);
    text-shadow: 0px 4px 4px rgba(0, 0, 0, 0.25);
    font-size: 2.5rem;
  }
`;
