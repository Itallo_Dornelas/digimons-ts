import { useFavoriteDigimons } from "../../providers/favorites";
import { ContainerCard, ContainerCards, Image } from "./styles";
import { AiFillStar } from "react-icons/all";
const FavoriteCard = () => {
  const { favorites, deleteDigimon } = useFavoriteDigimons();

  return (
    <>
      <h2>
        <AiFillStar />
        Favoritos
        <AiFillStar />
      </h2>
      <ContainerCards>
        {favorites.map((digimon, index) => {
          return (
            <ContainerCard key={index}>
              <AiFillStar />
              <h2>{digimon.name}</h2>
              <h4>{digimon.level}</h4>
              <Image src={digimon.img} />
              <button onClick={() => deleteDigimon(digimon)}>
                Remove Favorites
              </button>
            </ContainerCard>
          );
        })}
      </ContainerCards>
    </>
  );
};

export default FavoriteCard;
