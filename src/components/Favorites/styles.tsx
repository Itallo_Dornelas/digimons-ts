import styled from "styled-components";
export const ContainerCards = styled.div`
  display: flex;
  width: 100%;
  gap: 20px;
  flex-wrap: wrap;
`;
export const ContainerCard = styled.div`
  width: 20%;
  padding: 10px;
  margin: 0 auto;
  display: flex;
  flex-direction: column;
  align-items: center;
  background-color: var(--blue);
  border: 0.5px solid #ddd;
  border-radius: 20px;
  color: var(--orange);
  svg {
    font-size: 18px;
    color: var(--yellow);
  }
  button {
    margin-top: 10px;
    color: var(--white);
    letter-spacing: 0.1rem;
    text-transform: uppercase;
    background: var(--orange);
    background: -webkit-linear-gradient(to right, var(--yellow), var(--orange));
    background: linear-gradient(to right, var(--yellow), var(--orange));
    padding: 1rem 2rem;
    border-radius: 5px;
    cursor: pointer;
    border: none;

    :hover {
      color: rgba(0, 0, 0, 0.6);
    }
  }
`;

export const Image = styled.img`
  width: 70px;
  height: 70px;
  border-radius: 30px;
  object-fit: cover;
`;
