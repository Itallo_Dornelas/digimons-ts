import DigimonCard from "../../components/DigimonCard";
import Favorites from "../../components/Favorites";

import { Container } from "./styles";

const Home = () => {
  return (
    <Container>
      <DigimonCard />
      <Favorites />
    </Container>
  );
};

export default Home;
