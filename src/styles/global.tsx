import { createGlobalStyle } from "styled-components";
export default createGlobalStyle`
*{
    margin:0;
    padding: 0;
    box-sizing: border-box;
    outline: 0;
}
    :root{
            --white: #ffffff;
            --gray: #464353;
           --orange: #f4a261;
           --blue: #264653;
           --yellow: #e9c46a;
           --red: #e76f51;
        }
        
        
html,body{
    min-height: 100vh;
    &::-webkit-scrollbar {
    width: 10px;
    height: 5px;
  }

  ::-webkit-scrollbar-track {
    background: var(--white);
  }

  ::-webkit-scrollbar-thumb {
    background: var(--orange);
  }

  ::-webkit-scrollbar-thumb:hover {
    background: var(--gray);
  }
          
}
        
    body{
        background: var( --gray);
        color: var(--white);
          
    }
    body,input, button{
        font-family: 'Roboto', sans-serif;
        font-size: 1rem;

  }
    
    h1,h2,h3,h4,h5,h6{
        font-family: 'Roboto', sans-serif;
        font-weight: 700;
        text-align: center;
        margin: 10px;
    }
    button{
        cursor: pointer;
    }
    a{
        text-decoration: none;
    }
    ul,li,ol{
        list-style: none;
    }
    `;
